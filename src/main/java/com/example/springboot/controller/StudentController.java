package com.example.springboot.controller;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.springboot.dao.StudentRepository;
import com.example.springboot.model.Student;

@Controller
public class StudentController {
	
	@Autowired
	StudentRepository studentrepo;

	@RequestMapping("/")
	public String home() {
		return "home.jsp";
	}
	
	@RequestMapping("/addStudent")
	public String addStudent(Student student) {
		
		studentrepo.save(student);
		return "home.jsp";
	}
	
	@RequestMapping("/getStudent")
	public ModelAndView getStudent(@RequestParam int sid) {
		 
		ModelAndView mv= new ModelAndView("showstudent.jsp");
		Student student=studentrepo.findById(sid).orElse(new Student());
		//System.out.println(studentrepo.findByDepartment("CSE"));
		mv.addObject(student);
		return mv;
	}
	
	@RequestMapping("/Students")
	@ResponseBody
	public List<Student> getStudents()
	{
		return studentrepo.findAll();
	}
	
	@RequestMapping("/Students/{sid}")
	@ResponseBody
	public Optional<Student> getStudentById(@PathVariable ("sid") int sid)
	{
		return studentrepo.findById(sid);
	}
}
