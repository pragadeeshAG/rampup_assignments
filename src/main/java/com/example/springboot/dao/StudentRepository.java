package com.example.springboot.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.springboot.model.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>{

	List<Student> findByDepartment(String department);

}
