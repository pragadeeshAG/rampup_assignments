<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HomePage</title>


<!--  <link href="${contextPath}/resource/bootstrap.min.css" rel="stylesheet">-->
<style>
/* body{ */
/* background-color:lightblue; */

/* } */
body, html {
	height: 100%;
	margin: 0;
	font-family: Arial, Helvetica, sans-serif;
}

.bg-image {
	background-image: url("/images/img3.jfif");
	height: 100%;
	background-repeat: no-repeat;
	background-position: center;
	background-attachment: fixed;
	background-size: cover;
	filter: blur(3px);
	-webkit-filter: blur(2px);
	-moz-filter: blur(2px);
	-o-filter: blur(2px);
	-ms-filter: blur(2px);
}

h1 {
	text-align: center;
}

.contents {
	color: #eeeeee;
	font-weight: bold;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	width: 80%;
	padding: 40px;
	text-align: center;
	font-style:bold;
}

label {
	display: inline-block;
	width: 110px;
	font-family: Arial, Helvetica, sans-serif;
	font-style: bold;
	font-color:black;
}

input {
	padding: 5px 10px;
	font-style: bold;
}

.center {
	display: flex;
	justify-content: center;
	align-items: center;
}

.search {
	display: flex;
	justify-content: center;
	align-items: center;
}
</style>



</head>
<body>
	<!-- <img src="/images/student.jfif" height="200" width="250"> -->
<h1>
	<marquee>StudentRegistration</marquee>
</h1>
	<div class="bg-image"></div>
	<div class="contents">
		<form action="addStudent">
			<div>
				<label for="sid">StudentId:</label> <input id="sid" name="sid"
					type="text" autofocus /></br>
			</div>
			<div>
				<label for="studentname">StudentName:</label> <input
					id="studentname" name="studentname" type="text" /></br>
			</div>
			<div>
				<label for="gender">Gender:</label> <input id="gender" name="gender"
					type="text" /></br>
			</div>
			<div>
				<label for="department">Department:</label> <input id="department"
					name="department" type="text" /></br>
			</div>
			<div class="center">
				<input type="submit" value="Submit" /><br /></br>
				</br>
			</div>
		</form>

		<div class="search">
			<form action="getStudent">
				<div>
					<label for="sid">StudentId:</label> <input id="sid" name="sid"
						type="text" autofocus /></br>
				</div>
				<br /> <input type="submit"><br />
			</form>
		</div>
	</div>
</body>
</html>